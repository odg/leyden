# Leyden Firmware

This is the firmware that runs on the charge controller itself. It is written 
in [Rust](https://rust-lang.org), and can interface with a computer via a 
serial connection.

## Installing

The easiest way to install or update the fimware is to use the Leyden 
management program (in the [software](../software) folder).  
`leyden` will download the prebuilt firmware image, check its integrity by 
verifying its signature and checksum, and flash it to the microcontroller over 
a serial connection.

## Building

This should work in any POSIX environment (GNU/Linux, BSD, macOS, Cygwin, etc.)

To compile, you will need the Rust toolchain for the RISCV target.  
First, get [rustup](https://rustup.rs), either with your package manager or:

    curl https://sh.rustup.rs -sSf | sh

Then add the riscv32imac target:

    rustup target add riscv32imac-unknown-none-elf

You should now have the toolchain installed (`rustc` and `cargo`).  
Now build the firmware:

    cargo build

To check it compiled properly, compare the result of `sha512sum lpf.rom` to 
the checksums in the signed sha512sums file, the firmware should be 
[reproducible](http://reproducible-builds.org).

## Signing

Files will be signed with the PGP key of **odg@riseup.net**:  
[491E 0D9E E7AA 9E15 D089 950A 7879 6625 7046 CC21](https://pgp.mit.edu/pks/lookup?op=get&search=0x787966257046CC21)
