# LibrePower Hardware

This directory contains:

* The KiCad project for LibrePower (`librepower.pro`)
* A KiCad library with the necessary footprints (`fe310.lib`)
* A detailed bill of materials spreadsheet with pricing and part numbers 
  (`bom.ods`)
* PDFs of the complete schematics (`librepower.pdf`)
* Gerber files for the PCB layout (in `gerbers`)
* Renderings and photos of the PCB and the assembled board (in `images`)

[KiCad](http://kicad-pcb.org) is a free software electronics CAD suite, 
available for all main platforms. Install it to view or edit the hardware 
designs. KiCad can generate schematics, netlists, bill of materials, gerbers 
etc.

The BOM spreadsheet and schematic PDF should be viewable in any spreadsheet 
and PDF viewer respectively.

[This page](http://kicad-pcb.org/help/file-formats) is a useful reference for 
explaining the purpose of each file.

### Prototyping

For prototyping, it is easiest to use the 
[HiFive1](https://crowdsupply.com/sifive/hifive1) dev board to avoid needing 
to solder the QFP packaged chip, and a bench power supply or a premade PSU 
module rather than reinventing the wheel. On the right hand side of the 
`bom.ods` file I list some cheaper through hole components to use for a 
prototype or cheaper version of the board.

### Building

The parts can be ordered via Kitspace, to make it easier to get everything you 
need at once:  
<https://kitspace.org>

The PCB could be made at home or ordered from one of the many small volume PCB 
sellers online using the Gerber files.

### Resources

These are some freely available designs and specifications which are not 
necessary to build and use LibrePower.

Links to datasheets for all components are in the `bom.ods` file.

Documentation for the SiFive FE310 and the HiFive1 dev board can be found on 
[SiFive's site](https://www.sifive.com/documentation).

The HDL source of the SiFive FE310 microcontroller is 
[here](https://github.com/sifive/freedom), and the RISC V ISA specifications 
can be seen [here](https://riscv.org/specifications).
