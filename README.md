Leyden
======

**A free MPPT charge controller for renewable energy systems**

![OSHW](docs/images/oshw.png)
![GPL3](docs/images/gpl3.png)
![Rust](docs/images/rust.png)

The Leyden project is a flexible set of designs and code for a charge 
controller - a device that manages and monitors battery charging from 
renewable power sources like solar and wind. It does this as efficiently as 
possible to extract as much power as it can. Importantly, being free (libre), 
users have the right to inspect and modify the design, so it can be improved 
and adapt to many different environments.

---

Technical
---------

Leyden uses a 
[Maximum Power Point Tracking](https://wikipedia.org/wiki/Maximum_power_point_tracking)
algorithm to get the most power possible from **solar** (photovoltaic) panels, 
**wind** turbines, or potentially other renewable sources, to charge either 
**lead-acid** or **lithium-ion** batteries in various configurations. It can 
display various statistics about the batteries and power source, protects the 
batteries against over and under charging, can estimate the battery's 
[State of Charge](https://wikipedia.org/wiki/State_of_charge), and records 
data for logging and analysis purposes retrievable via a serial interface.

It uses an open source RISCV based 
[microcontroller](https://www.sifive.com/chip-designer#fe310) 
by SiFive, and is programmed in [Rust](https://rust-lang.org), a reliable, 
memory safe and efficient language.

Hardware design is done with [KiCad](http://kicad-pcb.org), an excellent free 
electronics CAD program.

Leyden is both completely free/libre 
[hardware](https://freedomdefined.org/OSHW) and 
[software](https://gnu.org/philosophy/free-sw).

Philosophy
----------

From the ground up the design aims to be as power efficient as possible, and 
use as few components as possible, not only to be environmentally concious, 
but in order to be a useful tool in making renewable energy accessible to 
everyone, by being easily and cheaply buildable anywhere. It aims to both be 
easy to produce and use, and buildable with commonly available parts, tools, 
with no royalties or expensive proprietary hardware/software. This could allow 
us to stop relying on cheaply made devices shipped from China that we can't 
inspect or modify, and use designs that are [open source appropriate 
technology](https://wikipedia.org/wiki/Open-source_appropriate_technology), 
allowing the user to adapt the design to their needs.  

Organisation
------------

This project is organised into these directories, see the `README.md` files in 
each for more information about the subprojects.  
All are GPLv3 licensed, see below for details.

* `/firmware` - Code for the firmware that runs on the microcontroller, 
  build instructions and prebuilt images.
* `/software` - Program which runs on a PC for configuring, monitoring, 
  updating, or getting data from the controller.
* `/hardware` - KiCad project with hardware designs, schematics and PCB layout. 
  Also a bill of materials and manual.
* `/docs` -     Documentation, notes, plans, theory of operation, images, etc.

License
-------

Copyright (C) 2018 Oliver Galvin

This applies to all files in this repository, see `COPYING` for the full text.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Email: **odg at riseup dot net**  
PGP: [491E 0D9E E7AA 9E15 D089 950A 7879 6625 7046 CC21](https://pgp.mit.edu/pks/lookup?op=get&search=0x787966257046CC21)
