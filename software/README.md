# Leyden

## Management Software

This is a small Python program which runs on your PC to interface with the 
charge controller.

It should run on any platform that Python supports, as long as you have the 
dependencies (see below).

It can be used to:

* Set the configuration options on the controller
* Check if there is a newer version of the firmware and update if necessary
* Generate and upload a list of sunset and sunrise times
* Download data logs from the controller
* Monitor the current charge level, panel voltage, temperature etc.

### Installation

The program uses Python, either get it from your package manager or 
[from here](https://python.org/downloads). Please note that I only test with 
Python 3, I can't guarantee it will work with earlier versions.

The easist way to install Leyden and its dependencies is with `pip`, Python's 
package manager:

    pip install leyden

To run without installing it, you first need these Python modules installed:

* GeoIP
* gnupg
* pyserial
* requests

You can get them with your package manager of choice, for example:

    apt install python3-geoip geoip-database-extra python3-gnupg python3-serial
    pacman -S python-geoip geoip-database-extra python-gnupg python-pyserial

Then you can run the program from within this directory:

    ./leyden.py

### Usage

See `leyden --help` for a list of available options.

It can also be used as a Python module, so could be integrated into scripts 
for data logging or automatic updating, for example.

### Connection

Leyden is deliberately agnostic regarding the PC connection method, to 
allow the builder as much choice as possible. By default the hardware just has 
a serial interface. If your PC doesn't have a serial connector you could 
connect to it with one of the 
[many](https://amazon.co.uk/s/?field-keywords=serial+to+usb+cable) cheap 
serial-USB cables/adapters. Or you could include a serial-USB chip with the 
charge controller, and just use a USB cable.

A fancier solution could be a wireless interface, for example using an 
[ESP8266 Wifi Module](https://wikipedia.org/wiki/ESP8266), an 
[XBee Wifi/RF module](https://www.digi.com/xbee), or a Bluetooth Low Energy 
module. These options would turn the controller into more of an IoT device, 
but would of course add a bit more complexity to the project. I haven't yet 
tried this kind of thing myself.
